# RiceMystery

This program tries to print out the number of rice corn on the last field of a check board like explained here:
https://en.wikipedia.org/wiki/Wheat_and_chessboard_problem

The solution should be 18.446.744.073.709.551.615 according to the wikipedia article.

This shall be a playground for me to try diffrent approaches I am finding in literature for this mathematical problem.

If you have any suggestions on how to improve the code, the workflow or documentation don't hesitate to open an issue.
 
