package de.nocompany.gotthold.main;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal("0");
        for (int i=0;i<64;i++) {
            bigDecimal = bigDecimal.add(BigDecimal.valueOf(Math.pow(2,i)));
            System.out.println(i + ": " + bigDecimal);
        }
        System.out.println("Ergebnis: " + bigDecimal.max(bigDecimal));
    }
}
